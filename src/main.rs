#[macro_use]
extern crate log;
extern crate env_logger;
extern crate mio;

mod utils;

use mio::*;
use mio::tcp::{TcpListener, TcpStream};
use std::io::{Read, Write};
use std::time::Duration;
use utils::stupidregistry::StupidRegistry;
use std::thread;
use std::sync::{atomic, Arc, Mutex};
use std::sync::mpsc::sync_channel;
use std::net::ToSocketAddrs;
use utils::helpers::*;
use utils::{reader, writer};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use utils::db::mysql_adapter::Attrs as db_attributes;
use utils::db::mysql_adapter::Handler as db;
use utils::db::mysql_adapter::Legend as db_legend;
use utils::db::db::Database;

fn main() {
    env_logger::init();
    let control_address = format!("{}:{}", "0.0.0.0", 9090).to_socket_addrs().unwrap().next().unwrap();
    let server = TcpListener::bind(&control_address).unwrap();
    info!("Binding to address {}", control_address);

    let poll_conn = Poll::new().unwrap();
    let poll_msg = Poll::new().unwrap();
    trace!("Polls initialised");

    let mut events_conn = Events::with_capacity(10);
    let mut events_msg = Events::with_capacity(100);
    trace!("Event handlers initialised");

    let script: Arc<Mutex<Vec<(u64, Vec<u8>)>>> = Arc::new(Mutex::new(Vec::with_capacity(50)));
    let script_writer = Arc::clone(&script);
    trace!("Script initialised");

    let state = Arc::new(atomic::AtomicUsize::new(0));
    let state_writer = Arc::clone(&state);
    trace!("Message queues initialised");

    let msg_queue: Arc<Mutex<Vec<String>>> = Arc::new(Mutex::new(Vec::with_capacity(100)));
    let msg_queue_writer = Arc::clone(&msg_queue);
    let msg_queue_reader = Arc::clone(&msg_queue);
    trace!("Message queues initialised");

    let (sync_tx, sync_rx) = sync_channel::<u32>(10000);
    trace!("Sync channel initialised");

    let drop_queue: Arc<Mutex<Vec<Token>>> = Arc::new(Mutex::new(Vec::with_capacity(100)));
    let drop_queue_writer = Arc::clone(&drop_queue);
    let drop_queue_reader = Arc::clone(&drop_queue);
    trace!("Drop queues initialised");

    let mut incoming = StupidRegistry::new(10);
    let clients_writer = Arc::new(Mutex::new(StupidRegistry::new(30)));
    let clients_reader = Arc::new(Mutex::new(StupidRegistry::new(30)));
    trace!("Stupid registries initialised");

    let park_flag_writer = Arc::new(atomic::AtomicBool::new(true));
    let park_flag_writer_clone = Arc::clone(&park_flag_writer);
    let park_flag_reader = Arc::new(atomic::AtomicBool::new(true));
    let park_flag_reader_clone = Arc::clone(&park_flag_reader);
    trace!("Flags initialised");

    let database: Arc<Mutex<Option<db>>> = Arc::new(Mutex::new(None));
    let database_reader = Arc::clone(&database);
    trace!("Database initialised");

    let clients_writer_cl = Arc::clone(&clients_writer);
    let clients_reader_cl = Arc::clone(&clients_reader);
    let writer = thread::spawn(move || {
        writer::writer_logic(park_flag_writer_clone,
                             clients_writer_cl,
                             script_writer,
                             state_writer,
                             msg_queue_writer,
                             drop_queue_writer,
                             sync_tx);
    });
    trace!("Spawning writer thread");

    let reader = thread::spawn(move || {
        reader::reader_logic(park_flag_reader_clone,
                             clients_reader_cl,
                             msg_queue_reader,
                             drop_queue_reader,
                             sync_rx,
                             database_reader);
    });
    trace!("Spawning reader thread");

    poll_conn.register(&server, Token(0), Ready::readable() | Ready::writable(), PollOpt::edge()).unwrap();
    let mut raw_buf = Vec::with_capacity(1000);
    let mut drop_queue_empty: bool;

    trace!("Entering control loop");
    loop {
        poll_conn.poll(&mut events_conn, Some(Duration::from_millis(1))).unwrap();
        for _ in events_conn.iter() {
            let (connection, address) = server.accept().unwrap();
            info!("Accepting connection from {}", address);
            let token = incoming.push(connection, address);
            debug!("Assigning token {:?} to address {} ", token, address);
            let &(ref connection, _) = incoming.get(&token).unwrap();
            poll_msg.register(connection, token, Ready::readable() | Ready::writable() | unix::UnixReady::hup(), PollOpt::edge()).unwrap();
            trace!("{} registered in poll", address);
        }

        {
            let guard = drop_queue.lock().unwrap();
            drop_queue_empty = guard.len() == 0;
        }

        if !drop_queue_empty {
            let drop;
            {
                let guard = drop_queue.lock().unwrap();
                trace!("Control thread acquired drop queue mutex");
                drop = guard.clone();
            }
            trace!("Control thread dropped drop queue mutex");
            let previous_wr = park_flag_writer.load(atomic::Ordering::Relaxed);
            let previous_rd = park_flag_reader.load(atomic::Ordering::Relaxed);
            park_flag_writer.store(true, atomic::Ordering::Relaxed);
            park_flag_reader.store(true, atomic::Ordering::Relaxed);
            trace!("Stop flags raised");
            for token in drop.iter() {
                {
                    let mut guard_writer = clients_writer.lock().unwrap();
                    trace!("Control thread acquired writer stupid registry mutex");
                    guard_writer.pop(token);
                    trace!("HUP connection removed from writer stupid registry");
                }
                trace!("Control thread dropped writer stupid registry mutex");
                {
                    let mut guard_reader = clients_reader.lock().unwrap();
                    trace!("Control thread acquired reader stupid registry mutex");
                    guard_reader.pop(token);
                    trace!("HUP connection removed from reader stupid registry");
                }
                trace!("Control thread dropped reader stupid registry mutex");
            }

            {
                let mut guard = drop_queue.lock().unwrap();
                trace!("Control thread acquired drop queue mutex");
                guard.clear();
            }
            trace!("Control thread dropped drop queue mutex");

            if !previous_wr {
                park_flag_writer.store(false, atomic::Ordering::Relaxed);
                trace!("Writer stop flag restored to false");
                writer.thread().unpark();
                trace!("Writer thread unparked");
            }
            if !previous_rd {
                park_flag_reader.store(false, atomic::Ordering::Relaxed);
                trace!("Reader stop flag restored to false");
                reader.thread().unpark();
                trace!("Reader thread unparked");
            }
        }

        poll_msg.poll(&mut events_msg, Some(Duration::from_millis(1))).unwrap();
        for event in events_msg.iter() {
            if event.readiness().contains(unix::UnixReady::hup()) {
                info!("HUP detected for token {:?}", event.token());
                let (connection, address) = incoming.pop(&event.token()).unwrap();
                trace!("Address {} popped from stupid registry", address);
                poll_msg.deregister(&connection).unwrap();
                trace!("Address {} deregistered in poll", address);
            } else {
                let &mut (ref mut control, address) = incoming.get_mut(&event.token()).unwrap();
                let _rc = control.read_to_end(&mut raw_buf);
                trace!("Read from {} into buffer", address);
                while raw_buf.len() > 0 {
                    let mut buf = peal_off(&mut raw_buf, vec!(b'\n', b'\0'));
                    strip(&mut buf);
                    let strg = peal_off(&mut buf, vec!(b' '));
                    let command = strg.as_slice();
                    debug!("Issued command {:?}", command);
                    match command {
                        b"start" => {
                            let strg = peal_off(&mut buf, vec!(b' '));
                            let command = strg.as_slice();
                            match command {
                                b"writer" => {
                                    park_flag_writer.store(false, atomic::Ordering::Relaxed);
                                    trace!("Writer stop flag set to false");
                                    writer.thread().unpark();
                                }
                                b"oneshot" => {
                                    writer.thread().unpark();
                                }
                                b"reader" => {
                                    {
                                        trace!("Flushing input buffers");
                                        let mut guard_reader = clients_reader.lock().unwrap();
                                        trace!("Control thread acquired reader stupid registry mutex");
                                        for entry in guard_reader.iter_mut() {
                                            let mut buf = Vec::with_capacity(1000);
                                            let (_, &mut (ref mut stream, _)) = entry;
                                            stream.read_to_end(&mut buf);
                                        }
                                    }
                                    trace!("Control thread dropped reader stupid registry mutex");
                                    park_flag_reader.store(false, atomic::Ordering::Relaxed);
                                    trace!("Reader stop flag set to false");
                                    reader.thread().unpark();
                                }
                                b"all" => {
                                    {
                                        trace!("Flushing input buffers");
                                        let mut guard_reader = clients_reader.lock().unwrap();
                                        trace!("Control thread acquired reader stupid registry mutex");
                                        for entry in guard_reader.iter_mut() {
                                            let mut buf = Vec::with_capacity(1000);
                                            let (_, &mut (ref mut stream, _)) = entry;
                                            stream.read_to_end(&mut buf);
                                        }
                                    }
                                    trace!("Control thread dropped reader stupid registry mutex");
                                    park_flag_writer.store(false, atomic::Ordering::Relaxed);
                                    trace!("Writer stop flag set to false");
                                    park_flag_reader.store(false, atomic::Ordering::Relaxed);
                                    trace!("Reader stop flag set to false");
                                    reader.thread().unpark();
                                    writer.thread().unpark();
                                }
                                _ => { control.write(b"Error: unknown start command\n"); }
                            }
                        }
                        b"stop" => {
                            let strg = peal_off(&mut buf, vec!(b' '));
                            let command = strg.as_slice();
                            match command {
                                b"writer" => {
                                    park_flag_writer.store(true, atomic::Ordering::Relaxed);
                                    trace!("Writer stop flag raised");
                                }
                                b"reader" => {
                                    park_flag_reader.store(true, atomic::Ordering::Relaxed);
                                    trace!("Reader stop flag raised");
                                }
                                b"all" => {
                                    park_flag_writer.store(true, atomic::Ordering::Relaxed);
                                    trace!("Writer stop flag raised");
                                    park_flag_reader.store(true, atomic::Ordering::Relaxed);
                                    trace!("Reader stop flag raised");
                                }
                                _ => { control.write(b"Error: unknown stop command\n"); }
                            }
                        }
                        b"status" => {
                            let mut msg = state.load(atomic::Ordering::Relaxed).to_string();
                            msg.push('\n');
                            control.write(msg.as_bytes());
                        }
                        b"table" => {
                            let strg = peal_off(&mut buf, vec!(b' '));
                            let command = strg.as_slice();
                            debug!("Issued table command {:?}", command);
                            match command {
                                b"add" => {
                                    let strg = peal_off(&mut buf, vec!(b' '));
                                    match String::from_utf8(strg)
                                        .unwrap_or(String::from(""))
                                        .parse::<u64>() {
                                        Ok(n) => {
                                            let mut hasher = DefaultHasher::new();
                                            trace!("Hasher initialised");

                                            strip(&mut buf);
                                            buf.hash(&mut hasher);
                                            let state = hasher.finish();
                                            let message = String::from_utf8(buf.clone()).unwrap_or(String::from(""));
                                            let mut hashed = state.to_string().into_bytes();
                                            hashed.push(b' ');
                                            hashed.append(&mut buf);

                                            let previous_wr = park_flag_writer.load(atomic::Ordering::Relaxed);
                                            let previous_rd = park_flag_reader.load(atomic::Ordering::Relaxed);
                                            park_flag_writer.store(true, atomic::Ordering::Relaxed);
                                            park_flag_reader.store(true, atomic::Ordering::Relaxed);
                                            trace!("Stop flags raised");
                                            {
                                                let mut guard = script.lock().unwrap();
                                                trace!("Control thread acquired script mutex");
                                                guard.push((n, hashed));
                                            }
                                            trace!("Control thread dropped script mutex");
                                            let msg = db_legend { state: format!("{}", state), message };
                                            {
                                                let mut guard = database.lock().unwrap();
                                                trace!("Control thread acquired database mutex");
                                                match *guard {
                                                    Some(ref mut t) => {
                                                        match t.push_legend(msg) {
                                                            Ok(_) => {}
                                                            Err(e) => {
                                                                warn!("Cannot push to legend: {}", e);
                                                            }
                                                        }
                                                    }
                                                    None => {}
                                                }
                                            }
                                            trace!("Control thread dropped database mutex");
                                            if !previous_wr {
                                                park_flag_writer.store(false, atomic::Ordering::Relaxed);
                                                trace!("Writer stop flag restored to false");
                                                writer.thread().unpark();
                                                trace!("Writer thread unparked");
                                            }
                                            if !previous_rd {
                                                park_flag_reader.store(false, atomic::Ordering::Relaxed);
                                                trace!("Reader stop flag restored to false");
                                                reader.thread().unpark();
                                                trace!("Reader thread unparked");
                                            }
                                        }
                                        Err(_) => {
                                            control.write(b"Error: time delay parse error\n");
                                        }
                                    }
                                }
                                b"clear" => {
                                    let previous_wr = park_flag_writer.load(atomic::Ordering::Relaxed);
                                    let previous_rd = park_flag_reader.load(atomic::Ordering::Relaxed);
                                    park_flag_writer.store(true, atomic::Ordering::Relaxed);
                                    park_flag_reader.store(true, atomic::Ordering::Relaxed);
                                    trace!("Stop flags raised");
                                    {
                                        let mut guard = script.lock().unwrap();
                                        trace!("Control thread acquired script mutex");
                                        guard.clear();
                                    }
                                    if !previous_wr {
                                        park_flag_writer.store(false, atomic::Ordering::Relaxed);
                                        trace!("Writer stop flag restored to false");
                                        writer.thread().unpark();
                                        trace!("Writer thread unparked");
                                    }
                                    if !previous_rd {
                                        park_flag_reader.store(false, atomic::Ordering::Relaxed);
                                        trace!("Reader stop flag restored to false");
                                        reader.thread().unpark();
                                        trace!("Reader thread unparked");
                                    }
                                }
                                _ => { control.write(b"Error: unknown table command\n"); }
                            }
                        }
                        b"connect" => {
                            let host = String::from_utf8(
                                peal_off(&mut buf, vec!(b' ')))
                                .unwrap_or(String::from(""));

                            let port = String::from_utf8(
                                peal_off(&mut buf, vec!(b' ')))
                                .unwrap_or(String::from(""));

                            trace!("Connecting to {}:{}", host, port);

                            match format!("{}:{}", host, port).to_socket_addrs() {
                                Ok(ref mut iter) => {
                                    match iter.next() {
                                        Some(addr) => {
                                            if addr == control_address {
                                                control.write(b"Error: illegal address\n");
                                            } else {
                                                let mut token: Option<mio::Token>;
                                                let previous_wr = park_flag_writer.load(atomic::Ordering::Relaxed);
                                                let previous_rd = park_flag_reader.load(atomic::Ordering::Relaxed);
                                                park_flag_writer.store(true, atomic::Ordering::Relaxed);
                                                park_flag_reader.store(true, atomic::Ordering::Relaxed);
                                                trace!("Stop flags raised");
                                                {
                                                    let guard_writer = clients_writer.lock().unwrap();
                                                    trace!("Control thread acquired writer stupid registry mutex");
                                                    token = guard_writer.get_token(addr);
                                                    trace!("Associated Token for {} in writer registry is {:?}", addr, token);
                                                }
                                                trace!("Control thread dropped writer stupid registry mutex");

                                                match token {
                                                    Some(_) => { control.write(b"Error: already connected\n"); }
                                                    None => {
                                                        match TcpStream::connect(&addr) {
                                                            Ok(mut client) => {
                                                                let token: mio::Token;
                                                                match client.set_nodelay(true) {
                                                                    Ok(_) => {}
                                                                    Err(e) => { warn!("Cannot set nodelay: {}", e) }
                                                                }
                                                                let client_clone = client.try_clone().unwrap();
                                                                {
                                                                    let mut guard_writer = clients_writer.lock().unwrap();
                                                                    trace!("Control thread acquired writer stupid registry mutex");
                                                                    token = guard_writer.push(client, addr.clone());
                                                                }
                                                                trace!("Control thread dropped writer stupid registry mutex");
                                                                {
                                                                    let mut guard_reader = clients_reader.lock().unwrap();
                                                                    trace!("Control thread acquired reader stupid registry mutex");
                                                                    guard_reader.push_with_key(token, client_clone, addr.clone());
                                                                }
                                                                trace!("Control thread dropped reader stupid registry mutex");
                                                            }
                                                            Err(_) => { control.write(b"Error: cannot connect\n"); }
                                                        }
                                                    }
                                                }
                                                if !previous_wr {
                                                    park_flag_writer.store(false, atomic::Ordering::Relaxed);
                                                    trace!("Writer stop flag restored to false");
                                                    writer.thread().unpark();
                                                    trace!("Writer thread unparked");
                                                }
                                                if !previous_rd {
                                                    park_flag_reader.store(false, atomic::Ordering::Relaxed);
                                                    trace!("Reader stop flag restored to false");
                                                    reader.thread().unpark();
                                                    trace!("Reader thread unparked");
                                                }
                                            }
                                        }
                                        None => {
                                            control.write(b"Error: cannot connect\n");
                                        }
                                    }
                                }
                                Err(_) => {
                                    control.write(b"Error: cannot resolve hostname\n");
                                }
                            }
                        }
                        b"disconnect" => {
                            let host = String::from_utf8(
                                peal_off(&mut buf, vec!(b' ')))
                                .unwrap_or(String::from(""));

                            let port = String::from_utf8(
                                peal_off(&mut buf, vec!(b' ')))
                                .unwrap_or(String::from(""));

                            trace!("Disconnecting from {}:{}", host, port);

                            match format!("{}:{}", host, port).to_socket_addrs() {
                                Ok(ref mut iter) => {
                                    match iter.next() {
                                        Some(addr) => {
                                            let mut token: Option<mio::Token>;
                                            let previous_wr = park_flag_writer.load(atomic::Ordering::Relaxed);
                                            let previous_rd = park_flag_reader.load(atomic::Ordering::Relaxed);
                                            park_flag_writer.store(true, atomic::Ordering::Relaxed);
                                            park_flag_reader.store(true, atomic::Ordering::Relaxed);
                                            trace!("Stop flags raised");
                                            {
                                                let guard_writer = clients_writer.lock().unwrap();
                                                trace!("Control thread acquired writer stupid registry mutex");
                                                token = guard_writer.get_token(addr);
                                                trace!("Associated Token for {} in writer registry is {:?}", addr, token);
                                            }
                                            trace!("Control thread dropped writer stupid registry mutex");
                                            match token {
                                                Some(token) => {
                                                    {
                                                        let mut guard_writer = clients_writer.lock().unwrap();
                                                        trace!("Control thread acquired writer stupid registry mutex");
                                                        guard_writer.pop(&token);
                                                    }
                                                    trace!("Control thread dropped writer stupid registry mutex");
                                                    {
                                                        let mut guard_reader = clients_reader.lock().unwrap();
                                                        trace!("Control thread acquired writer stupid registry mutex");
                                                        guard_reader.pop(&token);
                                                    }
                                                    trace!("Control thread dropped reader stupid registry mutex");
                                                }
                                                None => { control.write(b"Error: already disconnected\n"); }
                                            }
                                            if !previous_wr {
                                                park_flag_writer.store(false, atomic::Ordering::Relaxed);
                                                trace!("Writer stop flag restored to false");
                                                writer.thread().unpark();
                                                trace!("Writer thread unparked");
                                            }
                                            if !previous_rd {
                                                park_flag_reader.store(false, atomic::Ordering::Relaxed);
                                                trace!("Reader stop flag restored to false");
                                                reader.thread().unpark();
                                                trace!("Reader thread unparked");
                                            }
                                        }
                                        None => {
                                            control.write(b"Error: cannot connect\n");
                                        }
                                    }
                                }
                                Err(_) => {
                                    control.write(b"Error: cannot resolve hostname\n");
                                }
                            }
                        }
                        b"database" => {
                            let strg = peal_off(&mut buf, vec!(b' '));
                            let command = strg.as_slice();
                            match command {
                                b"connect" => {
                                    let mut ip = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut ip);
                                    let mut port = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut port);
                                    let mut user = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut user);
                                    let mut password = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut password);
                                    let db_attrs = db_attributes {
                                        ip: match String::from_utf8(ip) {
                                            Ok(t) => if t.len() > 0 { Some(t) } else { None },
                                            Err(_) => None,
                                        },
                                        port: String::from_utf8(port).unwrap_or(
                                            String::from("3306"))
                                            .parse().unwrap_or(3306),
                                        user: match String::from_utf8(user) {
                                            Ok(t) => if t.len() > 0 { Some(t) } else { None },
                                            Err(_) => None,
                                        },
                                        password: match String::from_utf8(password) {
                                            Ok(t) => if t.len() > 0 { Some(t) } else { None },
                                            Err(_) => None,
                                        },
                                    };
                                    match db::init(db_attrs) {
                                        Ok(t) => {
                                            let mut guard = database.lock().unwrap();
                                            trace!("Control thread acquired database mutex");
                                            *guard = Some(t);
                                        }
                                        Err(e) => {
                                            control.write(b"Error: cannot connect to database\n");
                                            warn!("Cannot init database: {}", e);
                                        }
                                    }
                                    trace!("Control thread dropped database mutex");
                                }
                                b"disconnect" => {
                                    {
                                        let mut guard = database.lock().unwrap();
                                        trace!("Control thread acquired database mutex");
                                        *guard = None;
                                    }
                                    trace!("Control thread dropped database mutex");
                                }
                                b"create" => {
                                    let mut strg = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut strg);
                                    let name = String::from_utf8(strg).unwrap_or(String::from(""));
                                    {
                                        let mut guard = database.lock().unwrap();
                                        trace!("Control thread acquired database mutex");
                                        match *guard {
                                            Some(ref mut db) => {
                                                match db.create_db(name.as_str()) {
                                                    Ok(_) => {
                                                        match db.select_db(name.as_str()) {
                                                            Ok(_) => {
                                                                match db.create_table_message() {
                                                                    Ok(_) => {}
                                                                    Err(e) => {
                                                                        control.write(b"Error: cannot create table message\n");
                                                                        warn!("Cannot create table message: {}", e);
                                                                    }
                                                                }
                                                                match db.create_table_legend() {
                                                                    Ok(_) => {}
                                                                    Err(e) => {
                                                                        control.write(b"Error: cannot create table legend\n");
                                                                        warn!("Cannot create table legend: {}", e);
                                                                    }
                                                                }
                                                            }
                                                            Err(e) => {
                                                                control.write(b"Error: cannot select db\n");
                                                                warn!("Cannot select db: {}", e);
                                                            }
                                                        }
                                                    }
                                                    Err(e) => {
                                                        control.write(b"Error: cannot create db\n");
                                                        warn!("Cannot create db: {}", e);
                                                    }
                                                }
                                            }
                                            None => { control.write(b"Error: no db connected\n"); }
                                        }
                                    }
                                    trace!("Control thread dropped database mutex");
                                }
                                b"drop" => {
                                    let mut strg = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut strg);
                                    let name = String::from_utf8(strg).unwrap_or(String::from(""));
                                    {
                                        let mut guard = database.lock().unwrap();
                                        trace!("Control thread acquired database mutex");
                                        match *guard {
                                            Some(ref mut db) => {
                                                match db.drop_db(name.as_str()) {
                                                    Ok(_) => {}
                                                    Err(_) => { control.write(b"Error: cannot create db\n"); }
                                                }
                                            }
                                            None => { control.write(b"Error: no db connected\n"); }
                                        }
                                    }
                                    trace!("Control thread dropped database mutex");
                                }
                                b"select" => {
                                    let mut strg = peal_off(&mut buf, vec!(b' '));
                                    strip(&mut strg);
                                    let name = String::from_utf8(strg).unwrap_or(String::from(""));
                                    {
                                        let mut guard = database.lock().unwrap();
                                        trace!("Control thread acquired database mutex");
                                        match *guard {
                                            Some(ref mut db) => {
                                                match db.select_db(name.as_str()) {
                                                    Ok(_) => {}
                                                    Err(_) => { control.write(b"Error: cannot select db\n"); }
                                                }
                                            }
                                            None => { control.write(b"Error: no db connected\n"); }
                                        }
                                    }
                                    trace!("Control thread dropped database mutex");
                                }
                                _ => { control.write(b"Error: unknown databese command\n"); }
                            }
                        }
                        b"" => {}
                        _ => { control.write(b"Error: unknown command\n"); }
                    }
                }
            }
        }
    }
}
