use std::collections::BTreeSet;
use std::fmt::Display;

pub struct Buffer<T: Sized + Ord + Clone+ Display> {
    size: usize,
    buffer: BTreeSet<T>,
}

impl<T> Buffer<T> where T: Sized + Ord + Clone + Display {
    pub fn new(capacity: usize) -> Self {
        Buffer {
            size: capacity,
            buffer: BTreeSet::new(),
        }
    }

    pub fn push(&mut self, token: T) -> Result<(), ()> {
        let mut out = Err(());
        if self.buffer.len() < self.size {
            if self.buffer.insert(token.clone()) {
                out = Ok(());
            }
        }
        out
    }

    pub fn pop(&mut self, token: T) -> Result<(), ()> {
        let mut out = Err(());
        if self.buffer.remove(&token) {
            out = Ok(());
        }
        out
    }

    pub fn clear(&mut self) {
        self.buffer.clear();
    }
}