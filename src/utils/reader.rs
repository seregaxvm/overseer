extern crate mio;

use mio::*;
use std::sync::{atomic, Arc, Mutex};
use std::sync::mpsc::Receiver;
use std::thread;
use std::time::Duration;
use utils::stupidregistry::StupidRegistry;
use std::io::Read;
use utils::helpers::*;
use utils::db::mysql_adapter::Handler as db;
use utils::db::mysql_adapter::Message as db_message;
use utils::db::db::Database;
use utils::buffer::Buffer;
use std::collections::BTreeMap;

fn sync_to_buffer(buffer: &mut BTreeMap<mio::Token, Buffer<u32>>, rx: &mut Receiver<u32>) -> Option<Vec<Token>> {
    let mut drop = Vec::new();
    for message in rx.try_iter() {
        for (address, buf) in buffer.iter_mut() {
            match buf.push(message) {
                Ok(_) => {}
                Err(_) => {
                    drop.push(address.clone());
                }
            }
        }
    }
    if drop.len() > 0 {
        Some(drop)
    } else {
        None
    }
}

pub fn reader_logic(park_flag: Arc<atomic::AtomicBool>,
                    clients: Arc<Mutex<StupidRegistry>>,
                    messages: Arc<Mutex<Vec<String>>>,
                    drop_queue: Arc<Mutex<Vec<Token>>>,
                    mut sync_rx: Receiver<u32>,
                    database: Arc<Mutex<Option<db>>>) {
    trace!("Reader thread spawned");
    let poll = Poll::new().unwrap();
    let mut events = Events::with_capacity(1000);
    let mut raw_buf = Vec::with_capacity(10000);
    let period = Duration::from_millis(100);
    let mut sync_buffer = BTreeMap::new();
    let message_buffer_cap = 10000;
    let mut message_buffer: Vec<db_message> = Vec::with_capacity(message_buffer_cap);
    loop {
        trace!("Reader thread parking");
        thread::park();
        {
            let mut clients_guard = clients.lock().unwrap();
            trace!("Reader thread acquired reader stupid registry mutex");

            for (token, entry) in clients_guard.iter_mut() {
                let &mut (ref mut connection, ref address) = entry;
                match poll.register(connection, token.clone(), Ready::readable() | unix::UnixReady::hup(), PollOpt::edge()) {
                    Ok(_) => {}
                    Err(e) => { warn!("Could not register {} to poll: {}", address, e) }
                }
                sync_buffer.insert(token.clone(), Buffer::new(10000));
            }
            trace!("Reader thread connections registered in poll");

            loop {
                match sync_to_buffer(&mut sync_buffer, &mut sync_rx) {
                    Some(drop) => {
                        {
                            let mut drop_guard = drop_queue.lock().unwrap();
                            trace!("Reader thread acquired drop queue mutex");
                            for token in drop {
                                warn!("Buffer overflow for token {:?}", token);
                                drop_guard.push(token);
                            }
                        }
                    },
                    None    => {},
                }
                poll.poll(&mut events, Some(period)).unwrap();
                for event in events.iter() {
                    if event.readiness().contains(unix::UnixReady::hup()) {
                        info!("HUP detected for token {:?}", event.token());
                        {
                            let mut drop_guard = drop_queue.lock().unwrap();
                            trace!("Reader thread acquired drop queue mutex");
                            drop_guard.push(event.token());
                        }
                        trace!("Reader thread dropped drop queue mutex");
                    } else {
                        match clients_guard.get_mut(&event.token()) {
                            Some(entry) => {
                                let &mut (ref mut connection, ref mut address) = entry;
                                match connection.read_to_end(&mut raw_buf) {
                                    Ok(_) => {},
                                    Err(_) => {},
                                }
                                while raw_buf.len() > 0 {
                                    let mut buf = peal_off(&mut raw_buf, vec!(b'\n', b'\0'));
                                    strip(&mut buf);
                                    match sync_to_buffer(&mut sync_buffer, &mut sync_rx) {
                                        Some(drop) => {
                                            {
                                                let mut drop_guard = drop_queue.lock().unwrap();
                                                trace!("Reader thread acquired drop queue mutex");
                                                for token in drop {
                                                    warn!("Buffer overflow for token {:?}", token);
                                                    drop_guard.push(token);
                                                }
                                            }
                                        },
                                        None    => {},
                                    }
                                    let sync_mesg_incoming = String::from_utf8(peal_off(&mut buf, vec!(b' ')))
                                        .unwrap_or(String::from("0")).parse::<u32>().unwrap_or(0);
                                    match sync_buffer.get_mut(&event.token()) {
                                        Some(mut buffer) => {
                                            match buffer.pop(sync_mesg_incoming) {
                                                Ok(_) => {
                                                    let time = String::from_utf8(peal_off(&mut buf, vec!(b' ')))
                                                        .unwrap_or(String::from("0"))
                                                        .parse::<u64>().unwrap_or(0);
                                                    let state = String::from_utf8(peal_off(&mut buf, vec!(b' ')))
                                                        .unwrap_or(String::from("0"))
                                                        .parse::<u64>().unwrap_or(0);
                                                    let message = String::from_utf8(buf)
                                                        .unwrap_or(String::from(""));
                                                    if message.len() > 0 {
                                                        let msg = db_message { time, state, message, client: format!("{}", address) };
                                                        if message_buffer.len() < message_buffer_cap {
                                                            message_buffer.push(msg);
                                                        } else {
                                                            {
                                                                let mut guard = database.lock().unwrap();
                                                                trace!("Read thread acquired database mutex");
                                                                match *guard {
                                                                    Some(ref mut t) => {
                                                                        match t.push_messages(&mut message_buffer) {
                                                                            Ok(_) => {}
                                                                            Err(e) => { warn!("Cannot push message: {}", e) }
                                                                        };
                                                                        message_buffer.clear();
                                                                    }
                                                                    None => {}
                                                                }
                                                            }
                                                            trace!("Read thread dropped database mutex");
                                                        }
                                                    }
                                                },
                                                Err(_) => {
                                                    {
                                                        warn!("No such token in the buffer");
                                                        {
                                                            let mut drop_guard = drop_queue.lock().unwrap();
                                                            trace!("Reader thread acquired drop queue mutex");
                                                            drop_guard.push(event.token());
                                                        }
                                                        trace!("Reader thread dropped drop queue mutex");
                                                    }
                                                }
                                            }
                                        },
                                        None => {
                                            {
                                                let mut drop_guard = drop_queue.lock().unwrap();
                                                trace!("Reader thread acquired drop queue mutex");
                                                drop_guard.push(event.token());
                                            }
                                            trace!("Reader thread dropped drop queue mutex");
                                        },
                                    }
                                }
                            },
                            None => {},
                        }
                    }
                }
                if message_buffer.len() > 0 {
                    {
                        let mut guard = database.lock().unwrap();
                        trace!("Read thread acquired database mutex");
                        match *guard {
                            Some(ref mut t) => {
                                match t.push_messages(&mut message_buffer) {
                                    Ok(_) => {}
                                    Err(e) => { warn!("Cannot push message: {}", e) }
                                };
                                message_buffer.clear();
                            }
                            None => {}
                        }
                    }
                    trace!("Read thread dropped database mutex");
                }
                if park_flag.load(atomic::Ordering::Relaxed) {
                    break;
                }
            }
            for (_, entry) in clients_guard.iter_mut() {
                let &mut (ref mut connection, ref address) = entry;
                match poll.deregister(connection) {
                    Ok(_) => {},
                    Err(e) => { warn!("Could not deregister {} from poll: {}", address, e) },
                }
            }
            sync_buffer.clear();
            trace!("Reader thread poll cleared");
        }
        trace!("Reader thread dropped reader stupid registry mutex");
    }
}