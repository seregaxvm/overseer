extern crate mio;
extern crate rand;

use mio::*;
use std::sync::{atomic, Arc, Mutex};
use std::sync::mpsc::SyncSender;
use std::thread;
use utils::stupidregistry::StupidRegistry;
use std::io::Write;
use std::time::{SystemTime, UNIX_EPOCH, Duration};

pub fn writer_logic(park_flag: Arc<atomic::AtomicBool>,
                    clients: Arc<Mutex<StupidRegistry>>,
                    script: Arc<Mutex<Vec<(u64, Vec<u8>)>>>,
                    state: Arc<atomic::AtomicUsize>,
                    messages: Arc<Mutex<Vec<String>>>,
                    drop_queue: Arc<Mutex<Vec<Token>>>,
                    sync_tx: SyncSender<u32>) {
    trace!("Writer thread spawned");
    let period = Duration::from_millis(100);
    let mut counter: u32 = 0;
    let mut message: Vec<u8> = Vec::with_capacity(10000);
    loop {
        trace!("Writer thread parking");
        thread::park();
        {
            let mut clients_guard = clients.lock().unwrap();
            trace!("Writer thread acquired writer stupid registry mutex");
            let script_guard = script.lock().unwrap();
            trace!("Writer thread acquired script mutex");
            loop {
                state.store(0, atomic::Ordering::Relaxed);
                for line in script_guard.iter() {
                    state.fetch_add(1, atomic::Ordering::Relaxed);
                    counter += 1;
                    let &(ref time, ref payload) = line;
                    let start_time = SystemTime::now();
                    let since_the_epoch = start_time.duration_since(UNIX_EPOCH)
                        .expect("Time went backwards");
                    let time_in_ms = since_the_epoch.as_secs() * 1000 +
                        since_the_epoch.subsec_nanos() as u64 / 1_000_000;

                    match sync_tx.try_send(counter) {
                        Ok(_) => {},
                        Err(e)=> {warn!("Sync channel send failed with error {}", e);},
                    }

                    message.extend_from_slice(&format!("{}", counter).into_bytes());
                    message.push(b' ');
                    message.extend_from_slice(&time_in_ms.to_string().into_bytes());
                    message.push(b' ');
                    message.extend_from_slice(payload);
                    message.push(b'\n');

                    if clients_guard.iter().len() == 0 {
                        thread::sleep(Duration::from_millis(100));
                    }

                    for (token, entry) in clients_guard.iter_mut() {
                        let &mut (ref mut connection, ref _address) = entry;
                        match connection.write(&message) {
                            Ok(_) => {}
                            Err(e) => {
                                        {
                                            let mut drop_guard = drop_queue.lock().unwrap();
                                            trace!("Writer thread acquired drop queue mutex");
                                            drop_guard.push(token.clone());
                                        }
                                        trace!("Writer thread dropped drop queue mutex");
                                warn!("Write failed {}", e);
                            }
                        }
                    }
                    if park_flag.load(atomic::Ordering::Relaxed) {
                        break;
                    }
                    let mut time_to_sleep = Duration::from_millis(time.clone());
                    let since_the_start = SystemTime::now().duration_since(start_time)
                        .expect("Time went backwards");
                    time_to_sleep.checked_sub(since_the_start).unwrap_or(Duration::from_millis(0));
                    while {
                        match time_to_sleep.checked_sub(period) {
                            Some(t) => {
                                time_to_sleep = t;
                                thread::sleep(period);
                                !park_flag.load(atomic::Ordering::Relaxed)
                            }
                            None => {
                                thread::sleep(time_to_sleep);
                                false
                            }
                        }
                    } {}
                    message.clear();
                }
                if park_flag.load(atomic::Ordering::Relaxed) {
                    break;
                }
            }
        }
        trace!("Writer thread dropped writer stupid registry mutex");
        trace!("Writer thread dropped script mutex");
    }
}