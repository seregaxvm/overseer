pub trait Database {
    type Attributes: ?Sized;
    type DbResult: Sized;
    type ConnResult: Sized;
    type Message;
    type Legend;

    fn init(attributes: Self::Attributes) -> Self::ConnResult;
    fn select_db(&mut self, db: &str) -> Self::DbResult;
    fn create_db(&mut self, db: &str) -> Self::DbResult;
    fn create_table_message(&mut self) -> Self::DbResult;
    fn create_table_legend(&mut self) -> Self::DbResult;
    fn drop_table_message(&mut self) -> Self::DbResult;
    fn drop_table_legend(&mut self) -> Self::DbResult;
    fn drop_db(&mut self, db: &str) -> Self::DbResult;
    fn push_message(&mut self, msg: Self::Message) -> Self::DbResult;
    fn push_messages(&mut self, msgs: &mut Vec<Self::Message>) -> Self::DbResult;
    fn push_legend(&mut self, msg: Self::Legend) -> Self::DbResult;
}