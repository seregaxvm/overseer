extern crate mio;
extern crate rand;

use std::net::SocketAddr;
use std::collections::{HashMap, hash_map};
use std::usize;

type HKey = mio::Token;
type HVal = (mio::net::TcpStream, SocketAddr);

pub struct StupidRegistry {
    entries: HashMap<HKey, HVal>
}

impl StupidRegistry {
    pub fn new(cap: usize) -> Self {
        StupidRegistry { entries: HashMap::with_capacity(cap) }
    }

    pub fn pop(&mut self, token: &mio::Token) -> Option<HVal> {
        self.entries.remove(&token)
    }

    pub fn push(&mut self, connection: mio::net::TcpStream, address: SocketAddr) -> mio::Token {
        let mut num:usize;
        while {
            while {
                num = rand::random::<usize>();
                num == usize::MAX  // reserved for mio internal use
            } {}
            let token = self.entries.get(&mio::Token(num));
            token.is_some()
        } {}
        self.entries.insert(mio::Token(num), (connection, address));
        mio::Token(num)
    }

    pub fn push_with_key(&mut self, token: mio::Token, connection: mio::net::TcpStream, address: SocketAddr) -> mio::Token {
        self.entries.insert(token, (connection, address));
        token.clone()
    }

    pub fn get(&mut self, token: &mio::Token) -> Option<&HVal> {
        self.entries.get(token)
    }

    pub fn get_mut(&mut self, token: &mio::Token) -> Option<&mut HVal> {
        self.entries.get_mut(token)
    }

    pub fn get_token(&self, target_addr: SocketAddr) -> Option<mio::Token> {
        let mut target_tok = None;
        for (token, connection) in self.iter() {
            let &(_, address) = connection;
            if target_addr == address {
                target_tok = Some(token.clone());
            }
        }
        target_tok
    }

    pub fn iter(&self) -> hash_map::Iter<HKey, HVal>{
        self.entries.iter()
    }

    pub fn iter_mut(&mut self) -> hash_map::IterMut<HKey, HVal>{
        self.entries.iter_mut()
    }
}
