# Description

Overseer is the TCP based message multiplexer intended to use with loosely coupled measurement device networks.

Program is a daemon listening on the TCP port.
Many control programs may communicate with it using text based protocol.
Communication with clients and database is done via TCP as well.

```
      +---------+  +---------+
      |Control 1|  |Control 2|
      +----+----+  +----+----+
           |            |
           +----+  +----+
                |  |
             +--v--v--+   +--------+
             |Overseer+--->Database|
             +---+-+--+   +--------+
               | | |
    +----------+ | +----------+
    |            |            |
+---v----+   +---v----+   +---v----+
|Client 1|   |Client 2|   |Client 3|
+--------+   +--------+   +--------+

```

Messages are organised in message table with delay associated with each message. Delay is the time in milliseconds before program sends next message from message table. Messages from message table are send to all clients.

```
Message table
+-------------+
|Delay|Message|
+-------------+
|.....|.......|
+-------------+
```

Because each message sent to the clients contains new system configuration, each of these messages describe a state of the system and may be referred to as states.

Each message contains transmission token, timestamp, state token and message itself, separated by whitespaces.

```
+------------------------------------------+
|Transmission token|Timestamp|State|Message|
+------------------------------------------+
```

Each client must respond to transmission.
Response should contain at least [Transmission token|Timestamp|State] part of the received transmission. Additionally, response message may be appended.

If response transmission contains  response message, program will record it in database.

Database contains two tables: legend table and message table.

```
Legend table
+-------------+
|State|Message|
+-------------+
|.....|.......|
+-------------+

Message table
+-------------------------+
|Time|State|Client|Message|
+-------------------------+
|....|.....|......|.......|
+-------------------------+
```

In legend table program stores messages from message table and hashes of these messages which are referred to as states.

In message table program stores messages from clients. Each line contains client message in text form, client IP address and port, timestamp as microseconds elapsed from 1970 and state.

Only MySQL database is supported.

# Protocol

- `client (connect|disconnect) <ip> <port>`	- connect to/disconnect from client
- `database connect <ip> <port> <user> <password>`	- connect to database
- `database (create|select|drop) <database_name>`	- create/select/drop database
- `status`	- current message table row (numbering from zero)
- `start (all|reader|writer)`	- start writer/reader part of program or both
- `stop (all|reader|writer)`	- stop writer/reader part of program or both
- `table add <timeout> <message>`	- append row to message table
- `table clear`	- clear message table
- `table`	- message table contents (not yet implemented)

# Installation

1. Install rust from [https://www.rust-lang.org/en-US/](https://www.rust-lang.org/en-US/)
1. Build program: `cargo build --release`
1. Run program: `cargo run --release`

# Docker image

Program may be run from docker image located at [https://hub.docker.com/r/matsievskiysv/overseer/](https://hub.docker.com/r/matsievskiysv/overseer/).

# Test run

Deploy database container:

`docker run -p 3306:3306 --name mysql --rm -e MYSQL_ROOT_PASSWORD=password -d mysql`

Deploy echo server:

`docker run --rm -d -p 8800:8800 --name echo krys/echo-server`

Deploy program:

`docker run -d --rm -p 9090:9090 --name overseer --link mysql:mysql --link echo:echo matsievskiysv/overseer`

To access program, use `netcat` program:

`nc 0.0.0.0 9090`

From `netcat` prompt:

```
database connect mysql 3306 root password
database create data
client connect echo 8800
table add 100 SET 1
table add 1000 POLL 1
table add 100 SET 2
table add 1000 POLL 2
start all
```

Now, database tables may be accessed:

`docker exec -it mysql /bin/bash`

From container prompt:

`mysql --password=password`

From `mysql` prompt:

`select * from data.message;`

